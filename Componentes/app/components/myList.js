import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar ,Image, Button } from 'react-native';

const DATA = [
  {
    id: '1',
    img: 'https://media.traveler.es/photos/61376ddacb06ad0f20e12713/16:9/w_1280,c_limit/143246.jpg',
    title: 'Arequipa',
    description: 'Arequipa es una ciudad del sur del Perú',
  },
  {
    id: '2',
    img: 'https://www.exploorperu.com/wp-content/uploads/2022/05/top-things-to-do-peru-exploor-peru-cusco.png',
    title: 'Cusco',
    description: 'Cusco es una ciudad del sur del Perú',
  },
  {
    id: '3',
    img: 'https://www.peru.travel/Contenido/Destino/Imagen/pe/37/1.3/Principal/Los%20Uros.jpg',
    title: 'Puno',
    description: 'Puno es una ciudad del sur del Perú',
  },
  {
    id: '4',
    img: 'https://www.esan.edu.pe/images/blog/2018/10/15/1500x844-lima-2035-2050.jpg',
    title: 'Lima',
    description: 'Lima es una ciudad del sur del Perú',
  },
  {
    id: '5',
    img: 'https://www.peru.travel/Contenido/Destino/Imagen/es/5/1.2/Principal/SouthAmericaPeru2017_1010_180704-5912_AGP_HDR.jpg',
    title: 'Ica',
    description: 'Ica es una ciudad del sur del Perú',
  },
];

const Item = ({ title , img , description }) => (
    <View style={styles.item}>
        <Image style={styles.img} source={{uri: img}}/>
        <View style={styles.item2}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.description}>{description}</Text>
            <Button style={styles.boton} title="Ver más"/>
        </View>
    </View>

);

const App1 = () => {
    const renderItem = ({ item }) => (
        <Item title={item.title} img={item.img} description={item.description} />
    );


  return (
    <SafeAreaView style={styles.container}>
        <View style={styles.contenido}>
        <FlatList style={styles.lista}
            data={DATA}
            renderItem={renderItem}
            keyExtractor={item => item.id}
        />
        </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    container: {
       
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#F3F2F2',
        padding: 10,
        marginVertical: 2,
        marginHorizontal: 20,
        width: '90%',
        height: 120,
        flexDirection: 'row',
    },
    
    title: {
        fontSize: 30,
    },
    img: {
        width: 100,
        height: 100,
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 50,
        marginRight: 25,
    },
    description: {
        fontSize: 11,
        color: 'gray',
    },
    
});

export default App1;
import React from "react";
import {View , Text, TextInput,StyleSheet, Button} from "react-native";

const AgeValidator = () => {
    const [number, onChangeNumber] = React.useState(null);
    var [text] = React.useState(null);

    if (number != null) {
        if (number < 18) {
            text = <Text style={{color: "red"}}>Eres menor de edad</Text>
        }
        else if (number == null) {
            text = 'Ingresa tu edad'
        }else{
            text = <Text style={{color: "green"}}>Eres mayor de edad</Text>
        }
    }
    return(
        <View style={styles.container}>
            <View style={styles.AgeValidator}>
                <Text>INGRESA TU EDAD</Text>
                <View style={styles.AgeValidator2}>
                    <TextInput style={styles.input}
                    onChangeText={onChangeNumber} value={number}
                    placeholder="Ingresar edad" keyboardType="numeric"/>
                    <Button title="Limpiar" onPress={() => onChangeNumber(null)}/>
                </View>
                <Text style = {styles.respuesta}>{text}</Text>
            </View>
        </View>
    )
    }

    const styles = StyleSheet.create({
        container: {
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
        },
        AgeValidator: {
            flex: 0.3,
            alignItems: 'center',
            justifyContent: 'center',

        },
        AgeValidator2: {
            flexDirection: 'row',
            alignItems: 'center',
        },
        
        input: {
            height: 40,
            width: '45%',
            borderColor: 'gray', 
            borderWidth: 2 ,
            alignItems: 'center',
            textAlign: 'center',
            margin: 10,
        },
        respuesta: {
            fontSize: 20,
            color: 'red',
            fontWeight: 'bold',
        },
    });

export default AgeValidator;
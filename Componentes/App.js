import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AgeValidator from './app/components/AgeValidator';
import App1 from './app/components/myList';


export default class App extends Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.AgeValidator}>
            <AgeValidator/>
        </View>
        <View style={styles.lineadivisora}/>
        <View style = {styles.myList}>
            <App1/>
        </View>
      </View>
      
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },

  AgeValidator: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  lineadivisora: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width: '100%',
  },

  myList: {
    flex: 0.7,
    width: '100%',
  },
});